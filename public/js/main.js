var mymap = L.map('mapid').setView([-38.93758741096065, -67.99292538719844], 10);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiZGV2YmNhMjAiLCJhIjoiY2tpMjd0dmJjMWVnbzJ5cXFnb2kwcXJqbyJ9.eN3c9Uv9VIYJDKGW2ZzDsg'
}).addTo(mymap);
$.ajax({
  dataType: "json",
  url:"api/bicicletas",
  success:function(result){
    result.bicicletas.forEach(function(bici){
      L.marker(bici.ubicacion,{title:bici.id}).addTo(mymap)
          .bindPopup(bici.modelo +"|"+ bici.color);
          /*.openPopup();*/

    });
  }
});
