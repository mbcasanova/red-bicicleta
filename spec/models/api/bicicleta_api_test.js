var Bicicleta = require('../../../models/bicicleta');
var request = require('request');
var server = require('../../../bin/www');
describe('Bicleta API',()=>{
  describe('GET BICICKLETAS /',()=>{
    it('Status 200',()=>{
      expect(Bicicleta.allBicis.length).toBe(0);
      var a = new Bicicleta(1, 'blanca', 'urbana', [-38.96, -68]);
      Bicicleta.add(a);
      request.get('http://localhost:3000/api/bicicletas', function(error, response, body){
          expect(response.statusCode).toBe(200);
      });
    });
  });
});
describe('Bicleta API',()=>{
  describe('POST BICICLETAS /create',()=>{
    it('Status 200',(done)=>{
      var headers = {'content-type':'application/json'};
      var a = '{"id":100,"color":"blanca","modelo":"urbana","lat":"-38.96","lng": "-68"}';
      request.post(
              {
                headers: headers,
                uri: 'http://localhost:3000/api/bicicletas/create',
                body: a
              },
              function(error,response,body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findByID(100).color).toBe('blanca');
                done();
              });
    });
  });
});

describe('Bicleta API',()=>{
  describe('POST BICICLETAS /update',()=>{
    it('Status 200',(done)=>{
      var headers = {'content-type':'application/json'};
      var a = '{"id":100,"color":"verde","modelo":"urbana","lat":"-38.96","lng": "-68"}';
      request.post(
              {
                headers: headers,
                uri: 'http://localhost:3000/api/bicicletas/update',
                body: a
              },
              function(error,response,body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findByID(100).color).toBe('verde');
                done();
              });
    });
  });
});


describe('Bicleta API',()=>{
  describe('POST BICICLETAS /delete',()=>{
    it('Status 200',(done)=>{
      var headers = {'content-type':'application/json'};
      var a = '{"id":100}';
      request.delete(
              {
                headers: headers,
                uri: 'http://localhost:3000/api/bicicletas/delete',
                body: a
              },
              function(error,response,body){
                expect(response.statusCode).toBe(204);
                for(var i=0; i < Bicicleta.allBicis.length; i++){
                  expect(Bicicleta.allBicis[i].id).not.toBe(100);
                }
                done();
              });
    });
  });
});
