var Bicicleta = require('../../models/bicicleta');
beforeEach(()=> {Bicicleta.allBicis=[];});
describe ('Bicicleta.allBicis',()=> {
  it('comienza vacio',()=>{
    expect(Bicicleta.allBicis.length).toBe(0);
  });
});
describe ('Bicicleta.add',()=> {
  it('agregamos una',()=>{
    expect(Bicicleta.allBicis.length).toBe(0);
    var a = new Bicicleta(2, 'verde', 'urbana', [-38.96, -68]);
    Bicicleta.add(a);
    expect(Bicicleta.allBicis[0]).toBe(a);
  });
});

describe ('Bicicleta.findByID',()=> {
  it('debe devolver la bici con id 1',()=>{
    expect(Bicicleta.allBicis.length).toBe(0);
    var a = new Bicicleta(1, 'blanca', 'urbana', [-38.96, -68]);
    var b = new Bicicleta(2, 'roja', 'montaña', [-38.96, -68]);
    Bicicleta.add(a);
    Bicicleta.add(b);
    var targetBici = Bicicleta.findByID(1);

    expect(targetBici.id).toBe(1);
    expect(targetBici.color).toBe('blanca');
    expect(targetBici.modelo).toBe('urbana');
  });
});

describe ('Bicicleta.removeById',()=> {
  it('debe remover la bici con id 1',()=>{
    expect(Bicicleta.allBicis.length).toBe(0);
    var a = new Bicicleta(1, 'blanca', 'urbana', [-38.96, -68]);
    var b = new Bicicleta(2, 'roja', 'montaña', [-38.96, -68]);
    Bicicleta.add(a);
    Bicicleta.add(b);
    var targetBici = Bicicleta.removeById(1);
    for(var i=0; i < Bicicleta.allBicis.length; i++){
      expect(Bicicleta.allBicis[i].id).not.toBe(1);  
    }
  });
});
